package itakademija.java2015.db.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {

	public static Connection establishConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:derby:TestDba;create=true", "root", "root");
	}

}
