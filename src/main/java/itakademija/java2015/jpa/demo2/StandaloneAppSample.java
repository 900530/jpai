package itakademija.java2015.jpa.demo2;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.demo1.Demo1;
import itakademija.java2015.jpa.demo1.repositories.BookRepository;
import itakademija.java2015.jpa.demo1.repositories.BookRepositoryJPA;

public class StandaloneAppSample {
	static final Logger log = LoggerFactory.getLogger(Demo1.class);


	/**
	 * Pvz. Kaip galima panaudoti JPA is standalone aplikacijos
	 */
	public void run() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("bookReviewDb");
		try {
			EntityManager entityManager = factory.createEntityManager();
			try {
				BookRepository bookRepo = new BookRepositoryJPA(entityManager);

				Book book = BookDbDataBuildHelper.buildSampleBook1();
				bookRepo.insertOrUpdate(book);
				
				
				book = BookDbDataBuildHelper.buildSampleBook2();
				bookRepo.insertOrUpdate(book);
				
				List<Book> books = bookRepo.findByTitleFragment("Java");
				for (Book b: books) {
					log.info("Found book: {}", b);
				}

			} finally {
				if (entityManager.isOpen())
					entityManager.close();
			}
		} finally {
			if (factory.isOpen())
				factory.close();
		}
	}

	public static void main(String args[]) {
		new StandaloneAppSample().run();
	}
}