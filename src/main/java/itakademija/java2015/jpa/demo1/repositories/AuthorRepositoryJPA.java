package itakademija.java2015.jpa.demo1.repositories;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import itakademija.java2015.jpa.assigment1.entities.Author;
import itakademija.java2015.jpa.assigment1.entities.Author_;

public class AuthorRepositoryJPA implements AuthorRepository {

	private EntityManager em;

	public AuthorRepositoryJPA(EntityManager entityManager) {
		this.em = entityManager;
	}

	/**
	 * Demonstruoju alternatyvų variantą, naudojantis tiesiogine užklausa JPQL (nėra type-safe)
	 */
	public Author findFirstByName(String name) {

		TypedQuery<Author> authorQuery = em
				.createQuery("SELECT a From Author a WHERE a.name = :name", Author.class);
		authorQuery.setParameter("name", name);
		authorQuery.setMaxResults(1);
		return authorQuery.getSingleResult();

	}

	/**
	 * Demonstruoju alternatyvų variantą, naudojantis Typesafe stiliumi.
	 */
	public Author findFirstByNameAlternative(String name) {

		 CriteriaBuilder cb = em.getCriteriaBuilder();
		 CriteriaQuery<Author> cq = cb.createQuery(Author.class);
		 Root<Author> bookRoot = cq.from(Author.class);
		 cq.select(bookRoot); // we select entity here
		 cq.where(cb.equal(bookRoot.get(Author_.name), name));
		 TypedQuery<Author> q = em.createQuery(cq);
		 q.setMaxResults(1);
		 return q.getSingleResult();
	}

}
