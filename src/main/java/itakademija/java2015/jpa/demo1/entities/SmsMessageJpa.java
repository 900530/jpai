package itakademija.java2015.jpa.demo1.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SmsMessageJpa {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String senderNumber;
	private String recipientNumber;
	private String text;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSenderNumber() {
		return senderNumber;
	}
	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}
	public String getRecipientNumber() {
		return recipientNumber;
	}
	public void setRecipientNumber(String recipientNumber) {
		this.recipientNumber = recipientNumber;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	@Override
	public String toString() {
		return "SmsMessageJpa [id=" + id + ", senderNumber=" + senderNumber + ", recipientNumber=" + recipientNumber
				+ ", text=" + text + "]";
	}
	
}
