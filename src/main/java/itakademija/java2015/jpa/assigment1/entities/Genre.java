package itakademija.java2015.jpa.assigment1.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Genre {
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToMany(mappedBy="genres")
	private List<Book> books;
	
	public Genre(){
		books = new ArrayList<Book>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}
//	Genre genre = new Genre();
//	genre.setName("thriller");
//
//	book.setGenres(new ArrayList<Genre>());
//	book.addGenre(genre);
	
}
