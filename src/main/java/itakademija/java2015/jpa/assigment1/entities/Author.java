package itakademija.java2015.jpa.assigment1.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Author {
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String lastname;

	@OneToMany(mappedBy="author")
	private List<Book> books;
	
//	@OneToOne(cascade=CascadeType.ALL)
	private Address address;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<Book> getBooks() { 
		return books;
	}
	public void addBook(Book b) {
		if (getBooks() == null)
			setBooks(new ArrayList<>());
		if (!getBooks().contains(b))
			getBooks().add(b);
	}
	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String surname) {
		this.lastname = surname;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", lastname=" + lastname + ", books=[...]" +  ", address="
				+ address + "]";
	}

}
