package itakademija.java2015.jpa.assigment1.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
public class Book {
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	private String title;
	
	@Temporal(TemporalType.DATE)
	private Date releaseDate;
	
	@Size(min = 1, max = 13)
	@Column(unique=true)
	private String ISBN;
	
	@JoinColumn(name="genre_id")
	@ManyToMany(cascade = CascadeType.ALL)
	private List<Genre> genres;
	
	public Book(){
		genres = new ArrayList<Genre>();
	}
	/**
	 * Edition of the book; For example: "second", "2nd", "anniversary", ..
	 */
	private String edition;
	@JoinColumn(name="author_id")
	@ManyToOne(optional=true, cascade={CascadeType.ALL})
	private Author author;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
		author.addBook(this);
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String ISBN) {
		this.ISBN = ISBN;
	}
	
	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", releaseDate=" + releaseDate + ", edition=" + edition + ", author=" + author
				+ "]";
	}
}
