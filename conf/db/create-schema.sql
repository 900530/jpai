create sequence hibernate_sequence start with 1 increment by 1
create table Author (id bigint not null, building varchar(255), city varchar(255), country varchar(255), flat varchar(255), street varchar(255), lastname varchar(255), name varchar(255), primary key (id))
create table Book (id bigint not null, ISBN varchar(255), edition varchar(255), releaseDate date, title varchar(255), author_id bigint, primary key (id))
create table SmsMessageJpa (id bigint not null, recipientNumber varchar(255), senderNumber varchar(255), text varchar(255), primary key (id))
create unique index UK_aa7tpa94hd9en14vdd7ed8vy2 on Book (ISBN)
alter table Book add constraint FK5gbo4o7yxefxivwuqjichc67t foreign key (author_id) references Author
