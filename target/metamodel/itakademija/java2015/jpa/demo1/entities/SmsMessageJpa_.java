package itakademija.java2015.jpa.demo1.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SmsMessageJpa.class)
public abstract class SmsMessageJpa_ {

	public static volatile SingularAttribute<SmsMessageJpa, String> senderNumber;
	public static volatile SingularAttribute<SmsMessageJpa, Long> id;
	public static volatile SingularAttribute<SmsMessageJpa, String> text;
	public static volatile SingularAttribute<SmsMessageJpa, String> recipientNumber;

}

